/**
 * generator for SimpleActivity - 2017.5.24
 *
 * keyword description
 *
 * d_pin = digital_pin, a_pin = analog_pin, p_pin = pwm_pin
 *
 */

'use strict';

/** In/Out Category **/
Blockly.JavaScript['high_low'] = function(block) {
    return [block.getFieldValue('state'), Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['digital_pin_list'] = function(block) {
    return [block.getFieldValue('digital_pin'), Blockly.JavaScript.ORDER_ATOMIC];
}

Blockly.JavaScript['analog_pin_list'] = function(block) {
    return [block.getFieldValue('analog_pin'), Blockly.JavaScript.ORDER_ATOMIC];
}

Blockly.JavaScript['pwm_pin_list'] = function(block) {
    return [block.getFieldValue('pwm_pin'), Blockly.JavaScript.ORDER_ATOMIC];
}

Blockly.JavaScript['digital_read'] = function(block) {
    var d_pin = Blockly.JavaScript.valueToCode(block, 'D_Pin',
                        Blockly.JavaScript.ORDER_ATOMIC);
    Blockly.JavaScript.setups_['setup_input' + d_pin] = 'pinMode(' + d_pin + ', INPUT);';
    var code = 'digitalRead(' + d_pin + ')';
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
}

Blockly.JavaScript['digital_write'] = function(block) {
    var d_pin = Blockly.JavaScript.valueToCode(block, 'D_Pin',
                        Blockly.JavaScript.ORDER_ATOMIC);
    var state = Blockly.JavaScript.valueToCode(block, 'state',
                        Blockly.JavaScript.ORDER_ATOMIC);
    Blockly.JavaScript.setups_['setup_output_' + d_pin] = 'pinMode(' + d_pin + ', OUTPUT);';
    var code = 'digitalWrite(' + d_pin + ', ' + state + ');\n';
    return code;
}

Blockly.JavaScript['analog_read'] = function(block) {
    var a_pin = Blockly.JavaScript.valueToCode(block, 'A_Pin',
                        Blockly.JavaScript.ORDER_ATOMIC);
    var code = 'analogRead(' + a_pin + ')';
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
}

Blockly.JavaScript['analog_write'] = function(block) {
    var a_pin = Blockly.JavaScript.valueToCode(block, 'A_Pin',
                        Blockly.JavaScript.ORDER_ATOMIC);
    var a_value = Blockly.JavaScript.valueToCode(block, 'A_Value',
                        Blockly.JavaScript.ORDER_ATOMIC);
    var code = 'analogWrite(' + a_pin + ', ' + a_value + ');\n';
    return code;
}


/** Control Category **/
Blockly.JavaScript['setup'] = function(block) {
    var branch = Blockly.JavaScript.statementToCode(block, 'DO');
    branch = branch.replace(/(^\s*)|(\s*$)/g, "");
    if (branch) {
        Blockly.JavaScript.setups_['setup_setup'] = branch;
    }
    return '';
}

Blockly.JavaScript['delay'] = function(block) {
    var delay_interval = Blockly.JavaScript.valueToCode(block, 'interval',
            Blockly.JavaScript.ORDER_ATOMIC);
    var delay_unit = block.getFieldValue('unit');
    var delay_func_type;
    if (delay_unit == 'millisecond') {
        delay_func_type = 'delay';
    } else if (delay_unit == 'microsecond') {
        delay_func_type = 'delayMicroseconds';
    }
    var code = delay_func_type + '(' + delay_interval + ');\n';
    return code;
};


/** SerialPort Category **/
Blockly.JavaScript['baud_rate'] = function(block) {
    var rate = Blockly.JavaScript.valueToCode(block, 'rate',
                Blockly.JavaScript.ORDER_ATOMIC) || '\"\"';
    Blockly.JavaScript.setups_['setup_serial_9600'] = 'Serial.begin(' + rate + ');\n';
    return '';
}

Blockly.JavaScript['println'] = function(block) {
    if (Blockly.JavaScript.setups_['setup_serial_9600']) {
    } else {
        Blockly.JavaScript.setups_['setup_serial_9600'] = 'Serial.begin(9600);\n';
    }
    var data = Blockly.JavaScript.valueToCode(block, 'println_data',
                Blockly.JavaScript.ORDER_NONE) || '\"\"';
    return 'Serial.println(' + data + ');\n';
};


/** Sensor Category **/
Blockly.JavaScript['dht11'] = function(block) {
    var sensor_type = '11'
    var d_pin = Blockly.JavaScript.valueToCode(block, 'D_Pin', Blockly.JavaScript.ORDER_ATOMIC);
    var temp_humi_mode = this.getFieldValue('temp_humi_mode');
    Blockly.JavaScript.definitions_['define_dht'] = '#include <dht.h>';
    Blockly.JavaScript.definitions_['var_dht_' + d_pin] = 'dht myDHT_' + d_pin + ';';
    var funcName = 'dht_' + d_pin + '_' + temp_humi_mode;
    var code = 'int' + ' ' + funcName + '() {\n'
        + '  int chk = myDHT_' + d_pin + '.read' + sensor_type + '(' + d_pin + ');\n'
        + '  int value = myDHT_' + d_pin + '.' + temp_humi_mode + ';\n'
        + '  return value;\n'
        + '}\n';
    Blockly.JavaScript.definitions_[funcName] = code;
    return [funcName + '()', Blockly.JavaScript.ORDER_ATOMIC];
}

Blockly.JavaScript['cds_sensor'] = function(block) {
    var a_pin = Blockly.JavaScript.valueToCode(block, 'A_Pin', Blockly.JavaScript.ORDER_ATOMIC);
    var code = '1023 - analogRead(' + a_pin + ')';
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
}

Blockly.JavaScript['max9812'] = function(block) {
    var a_pin = Blockly.JavaScript.valueToCode(block, 'A_Pin', Blockly.JavaScript.ORDER_ATOMIC);
    Blockly.JavaScript.definitions_['sampleWindow_define'] = 'const int sampleWindow = 50;';
    Blockly.JavaScript.definitions_['sample_var'] = 'unsigned int sample;';
    Blockly.JavaScript.definitions_['blank_line'] = '\n';
    var func_name = 'get_amplitude';
    var code = 'int ' + func_name + '()\n'
        + '{\n'
        + '  unsigned long startMillis= millis();\n'
        + '  unsigned int peakToPeak = 0;\n'
        + '  unsigned int signalMax = 0;\n'
        + '  unsigned int signalMin = 1024;\n'
        + '\n'
        + '  // collect data for 50ms\n'
        + '  while (millis() - startMillis < sampleWindow)\n'
        + '  {\n'
        + '    sample = analogRead(' + a_pin + ');\n'
        + '    signalMin = min(signalMin, sample);\n'
        + '    signalMax = max(signalMax, sample);\n'
        + '  }\n'
        + '\n'
        + '  peakToPeak = signalMax - signalMin;\n'
        + '  return peakToPeak;\n'
        + '}\n';
    Blockly.JavaScript.definitions_[func_name] = code;
    return ['get_amplitude()', Blockly.JavaScript.ORDER_ATOMIC];
}

Blockly.JavaScript['rcw0001'] = function(block) {
    Blockly.JavaScript.definitions_['trig_pin'] = 'int trigPin = 4;';
    Blockly.JavaScript.definitions_['echo_pin'] = 'int echoPin = 3;';
    Blockly.JavaScript.definitions_['blank_line'] = '\n';
    var func_name = 'get_distance';
    var code = 'int ' + func_name + '()\n'
        + '{\n'
        + '  long duration, distance;\n'
        + '  digitalWrite(trigPin, LOW);\n'
        + '  delayMicroseconds(2);\n'
        + '  digitalWrite(trigPin, HIGH);\n'
        + '  delayMicroseconds(10);\n'
        + '  digitalWrite(trigPin, LOW);\n'
        + '  duration = pulseIn(echoPin, HIGH);\n'
        + '  distance = duration * 17 / 1000;\n'
        + '  return distance;\n'
        + '}\n';
    Blockly.JavaScript.definitions_[func_name] = code;
    Blockly.JavaScript.setups_['trigPin_output_mode'] = 'pinMode(trigPin, OUTPUT);';
    Blockly.JavaScript.setups_['echoPin_input_mode'] = 'pinMode(echoPin, INPUT);';
    return ['get_distance()', Blockly.JavaScript.ORDER_ATOMIC];
}

Blockly.JavaScript['buzzer'] = function(block) {
    var buzzer_pin = Blockly.JavaScript.valueToCode(block, 'PWM_Pin', Blockly.JavaScript.ORDER_ATOMIC);
    Blockly.JavaScript.setups_['buzzerpin_output'] = 'pinMode(' + buzzer_pin + ', OUTPUT);';
    var code = 'analogWrite(' + buzzer_pin + ', HIGH);\n'
        + 'delay(1000);\n'
        + 'analogWrite(' + buzzer_pin + ', LOW);\n'
        + 'delay(1000);\n';
    return code;
}