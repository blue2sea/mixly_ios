/*
 * Copyright 2015 Google Inc. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import UIKit
import Blockly
import Foundation
import MessageUI


class SimpleWorkbenchViewController: WorkbenchViewController, MFMailComposeViewControllerDelegate {
    // MARK: - Initializers
    
    /// Code generator service
    var _codeGeneratorService: CodeGeneratorService = {
        // Create the code generator service
        let codeGeneratorService = CodeGeneratorService(
            jsCoreDependencies: [
                // The JS file containing the Blockly engine
                "SimpleWorkbench/blockly_compressed.js",
                // The JS file containing a list of internationalized messages
                "SimpleWorkbench/en.js"
            ])
        
        // Create builder for creating code generator service requests
        let builder = CodeGeneratorServiceRequestBuilder(
            // This is the name of the JS object that will generate JavaScript code
            jsGeneratorObject: "Blockly.JavaScript")
        builder.addJSBlockGeneratorFiles([
            // Use JavaScript code generators for the default blocks
            "SimpleWorkbench/javascript_compressed.js",
            // Use JavaScript code generators for our custom turtle blocks
            "SimpleWorkbench/blocklyduino_generators.js"])
        // Load the block definitions for all default blocks
        builder.addJSONBlockDefinitionFiles(fromDefaultFiles: .allDefault)
        // Load the block definitions for our custom blocks
        builder.addJSONBlockDefinitionFiles(["SimpleWorkbench/inout_blocks.json"])
        builder.addJSONBlockDefinitionFiles(["SimpleWorkbench/control_blocks.json"])
        builder.addJSONBlockDefinitionFiles(["SimpleWorkbench/serial_port_blocks.json"])
        builder.addJSONBlockDefinitionFiles(["SimpleWorkbench/sensor_blocks.json"])
        builder.addJSONBlockDefinitionFiles(["SimpleWorkbench/logic_blocks.json"])
        builder.addJSONBlockDefinitionFiles(["SimpleWorkbench/loop_blocks.json"])
        builder.addJSONBlockDefinitionFiles(["SimpleWorkbench/math_blocks.json"])
        builder.addJSONBlockDefinitionFiles(["SimpleWorkbench/text_blocks.json"])
        builder.addJSONBlockDefinitionFiles(["SimpleWorkbench/list_blocks.json"])
        builder.addJSONBlockDefinitionFiles(["SimpleWorkbench/colour_blocks.json"])
        builder.addJSONBlockDefinitionFiles(["SimpleWorkbench/variable_blocks.json"])
        
        // Set the request builder for the CodeGeneratorService.
        codeGeneratorService.setRequestBuilder(builder, shouldCache: true)
        
        return codeGeneratorService
    }()
    
    init() {
        super.init(style: .defaultStyle)
    }
    
    required init?(coder aDecoder: NSCoder) {
        assertionFailure("Called unsupported initializer")
        super.init(coder: aDecoder)
    }
    
    // MARK: - Super
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Don't allow the navigation controller bar cover this view controller
        self.edgesForExtendedLayout = UIRectEdge()
        self.navigationItem.title = "Blocks"
        
        // Load data
        loadBlockFactory()
        loadToolbox()
    }

    // send mail test
    func sendEmail() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients(["peterlampad@consultant.com"])
        composeVC.setSubject("good")
        composeVC.setMessageBody("this is message body", isHTML: false)
        self.present(composeVC, animated: true, completion: nil)
    }
    
    // file read test
    private func loadFile()->String {
        let filename = "/cores/a.ino"
        let content: String
        do {
            content = try String (contentsOfFile: filename, encoding: String.Encoding.utf8)
            print("Read success")
        } catch _ {
            content = ""
            print("Read failed")
        }
        return content
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    // MARK: - Private
    
    private func loadBlockFactory() {
        // Load blocks into the block factory
        blockFactory.load(fromDefaultFiles: .allDefault)
        
        // add json files
        do {
            try blockFactory.load(fromJSONPaths: ["SimpleWorkbench/inout_blocks.json"])
            try blockFactory.load(fromJSONPaths: ["SimpleWorkbench/control_blocks.json"])
            try blockFactory.load(fromJSONPaths: ["SimpleWorkbench/serial_port_blocks.json"])
            try blockFactory.load(fromJSONPaths: ["SimpleWorkbench/sensor_blocks.json"])
            try blockFactory.load(fromJSONPaths: ["SimpleWorkbench/logic_blocks.json"])
            try blockFactory.load(fromJSONPaths: ["SimpleWorkbench/loop_blocks.json"])
            try blockFactory.load(fromJSONPaths: ["SimpleWorkbench/math_blocks.json"])
            try blockFactory.load(fromJSONPaths: ["SimpleWorkbench/text_blocks.json"])
            try blockFactory.load(fromJSONPaths: ["SimpleWorkbench/list_blocks.json"])
            try blockFactory.load(fromJSONPaths: ["SimpleWorkbench/colour_blocks.json"])
            try blockFactory.load(fromJSONPaths: ["SimpleWorkbench/variable_blocks.json"])
        } catch let error {
            print("An error occurred loading the blocks: \(error)")
        }
    }
    
    private func loadToolbox() {
        // Create a toolbox
        do {
            let toolboxPath = "SimpleWorkbench/toolbox.xml"
            if let bundlePath = Bundle.main.path(forResource: toolboxPath, ofType: nil) {
                let xmlString = try String(contentsOfFile: bundlePath, encoding: String.Encoding.utf8)
                let toolbox = try Toolbox.makeToolbox(xmlString: xmlString, factory: blockFactory)
                try loadToolbox(toolbox)
            } else {
                print("Could not load toolbox XML from '\(toolboxPath)'")
            }
        } catch let error {
            print("An error occurred loading the toolbox: \(error)")
        }
    }
}
